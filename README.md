fasil
=====

CMS based on
*  html
*  ANgularJS
*  json
*  html
*  php

##No SQL needed.##

All content come from json.

Manage yout article easy.

##HowTo define own Sectiontemplates.##

open content/templates.json

Add new key:value

*be shure key is unique!*

e.g.:

```"TemplateName": "<a href='#'><img class='img-responsive' ng-src='{{content.data}}' alt=''></a>",``` 

your template can use up to two values:

*  content.data
*  content.data2

e.g.:

```"thumbnail": "<div><img class='img-responsive' ng-src='{{content.data}}' alt=''><p>{{content.data2}}</p></div>",```

if your template contains a image (img) you have to add the templateName to a array.

open js/controller.js ind find:

``` $scope.imageTemplates = ['img','thumbnail'];```

open content/templatesInfo.json

add new json line with your template name and description

e.g.:

 ```"thumbnail": "Image and Text",```

*be shure key is unique!*
