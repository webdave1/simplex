Dieser Script wurde runtergeladen von www.clanwissen.de

Dies ist ein einfacher Loginscript mit Sessions. Der Benutzername und das verschl�sselte Passwort wird in der user.txt gespeichert. 
F�r den erste Probelogin kann man sich mit dem Benutzername Clanwissen.de und dem Passwort clanwissen einloggen.

Falls du den Script online benutzt, bitte nenn die Passwort-Datei um, z.B. in 3hf5Hd3f5.txt  denn wenn Angreifer den md5-Hash (Wert) der Passworter haben, ist es f�r sie ein leichtes, sich als jemand anderes einzuloggen. Damit dies nicht passiert, sollten sie die Datei nicht zu Gesicht bekommen.

Falls es noch Fragen/Kritik/Anregungen gibt, bitte eine E-Mail an: info@clanwissen.de

Ihr d�rft den Script frei verwenden und auch hochladen. Aber bitte nicht auf der eigenen Site zum Download anbieten.

