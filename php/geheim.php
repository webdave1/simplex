<?php
session_start();
if (!isset($_SESSION['username'])) {
    echo "Bitte erst <a href=\"login.html\">einloggen</a>";
    exit;
}
?>

<html>
    <head>
        <title></title>
        <meta name="author" content="Andavos">
        <meta name="generator" content="Ulli Meybohms HTML EDITOR">
    </head>
    <body text="#000000" bgcolor="#FFFFFF" link="#FF0000" alink="#FF0000" vlink="#FF0000">
        Herzlich Willkommen im gesch&uuml;tzen Bereich
        
        <form action="img_upload.php" method="post" enctype="multipart/form-data">
            {
                    "Headline": "Man must explore, and this is exploration at its greatest",
                    "subtitle": "Problems look mighty small from 150 miles up",
                    "text":
                            [
                                {"content_type": "h2","data": "Headline"},
                                {"content_type": "thumbnail","data": "img/320x150.gif","data2": "A Chinese tale tells of some men sent to harm a young girl who, upon seeing her beauty, become her protectors rather than her violators. That's how I felt seeing the Earth for the first time. I could not help but love and cherish her.For those who have seen the Earth from space, and for the hundreds and perhaps thousands more who will, the experience most certainly changes your perspective. The things that we share in our world are far more valuable than those which divide us."},
                                {"content_type": "info","data": "What was most significant about the lunar voyage was not that man set foot on the Moon but that they set eye on the earth."},
                                {"content_type": "warning","data": "A Chinese tale tells of some men sent to harm a young girl who, upon seeing her beauty, become her protectors rather than her violators. That's how I felt seeing the Earth for the first time. I could not help but love and cherish her."},
                                {"content_type": "success","data": "For those who have seen the Earth from space, and for the hundreds and perhaps thousands more who will, the experience most certainly changes your perspective. The things that we share in our world are far more valuable than those which divide us."},
                                {"content_type": "h2","data": "The Final Frontier"},
                                {"content_type": "p","data": "There can be no thought of finishing for ‘aiming for the stars.’ Both figuratively and literally, it is a task to occupy the generations. And no matter how much progress one makes, there is always the thrill of just beginning."},
                                {"content_type": "danger","data": "There can be no thought of finishing for ‘aiming for the stars.’ Both figuratively and literally, it is a task to occupy the generations. And no matter how much progress one makes, there is always the thrill of just beginning."},
                                {"content_type": "blockquote","data": "The dreams of yesterday are the hopes of today and the reality of tomorrow. Science has not yet mastered prophecy. We predict too much for the next year and yet far too little for the next ten."},
                                {"content_type": "p","data": "Spaceflights cannot be stopped. This is not the work of any one man or even a group of men. It is a historical process which mankind is carrying out in accordance with the natural laws of human development."},
                                {"content_type": "h2","data": "Reaching for the Stars"},
                                {"content_type": "p","data": "As we got further and further away, it [the Earth] diminished in size. Finally it shrank to the size of a marble, the most beautiful you can imagine. That beautiful, warm, living object looked so fragile, so delicate, that if you touched it with a finger it would crumble and fall apart. Seeing this has to change a man."},
                                {"content_type": "img","data": "img/post-sample-image.jpg"},
                                {"content_type": "span","data": "To go places and do things that have never been done before – that’s what living is all about."},
                                {"content_type": "p","data": "Space, the final frontier. These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before."},
                                {"content_type": "p","data": "As I stand out here in the wonders of the unknown at Hadley, I sort of realize there’s a fundamental truth to our nature, Man must explore, and this is exploration at its greatest."}

                            ],  
                    "author": "webdave",
                    "date": "01.01.2014"
                }
            <input type="file" name="datei"><br>
            <input type="submit" value="Hochladen">
        </form>
    </body>
</html>