<?php
session_start();
if (!isset($_SESSION['username'])) {
    echo "Please <a class='btn btn-default' href=\"#/login\">login</a> first";
    exit;
}
?>    
<div class="row"  ng-init="">
    <div class="col-md-6">   
        welcome to Editor
        <hr>
        <iframe src="php/img_upload.html">
            <p>Your browser does not support iframes.</p>
        </iframe><!-- <image-uploader></image-uploader> -->
            <hr>
        <form ng-init="nextSection=[]">
            <div class="form-group">
                <label for="Headline">Headline</label>
                <input type="text" class="form-control" id="Headline" placeholder="Headline" ng-model="newTemplate.Headline" >
            </div>
            <div class="form-group">
                <label for="subtitle">subtitle</label>
                <input type="text" class="form-control" id="subtitle" placeholder="subtitle" ng-model="newTemplate.subtitle" >
            </div>
            <div class="form-group">
                <label for="newTextElement">Select format Template</label>
                <select ng-model="nextSection">
                    <option value="{{name}}" ng-repeat="(name,description) in templateInfos">{{description}}</option>
                </select>
            </div>
            <div class="form-group" ng-hide="((nextSection == 'img')||(nextSection == ''))">
                <label for="newTextElement">Text</label>
                <input type="text" class="form-control" id="newTextElement" placeholder="new Text Element" ng-model="newTextElement" >
            </div>
            <div class="form-group" ng-show="((nextSection == 'img')||(nextSection == 'thumbnail'))">
                <label for="newImageElement">Image</label>
                <select ng-model="nextImage">
                    <option value="{{iname}}" ng-repeat="(iId,iname) in imageList">{{iname}}</option>
                </select><span class="glyphicon glyphicon-refresh" ng-click="updateImageList()"></span>
            </div>
            <div class="form-group" ng-hide="(nextSection == '')">
                <button ng-click="addSection(nextSection,newTextElement,nextImage,editId);newTextElement='';nextImage='';editId=-1">Add Section</button>  
            </div>
            <div class="form-group">
                <label for="author">author</label>
                <input type="text" class="form-control" id="author" placeholder="author" ng-model="newTemplate.author" >
            </div>
        </form>
    </div>
    <div class="col-md-6 bordered post-preview">
        <h2 class="post-title">{{newTemplate.Headline}}</h2>
        <h3 class="post-subtitle">{{newTemplate.subtitle}}</h3>
        <p class="post-meta">Posted by {{newTemplate.author}} on {{newTemplate.date | date:'yyyy-MM-dd'}}</p>
        <hr>
        <demo-item  ng-repeat-start="(content_type,data) in newTemplate.text" id="{{$index}}" content="data"></demo-item >
        <button type='button' class='btn btn-default' aria-label='Left Align'  ng-click='$parent.nextSection=newTemplate.text[$index].content_type;(imageTemplates.indexOf(newTemplate.text[$index].content_type) != -1)?$parent.newTextElement=newTemplate.text[$index].data2:$parent.newTextElement=newTemplate.text[$index].data;(imageTemplates.indexOf(newTemplate.text[$index].content_type) != -1)?$parent.nextImage=newTemplate.text[$index].data:$parent.nextImage=""; $parent.editId=$index;'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></button>
        <button ng-repeat-end type='button' class='btn btn-default' aria-label='Left Align'  ng-click='deleteSection($index);editId=-1'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>
        <hr>
        <button class="btn btn-default" ng-click="saveSection()">Save</button>              
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        
    </div>
    <div class="col-md-6">
        <button class="btn btn-default" ng-click="logout()">Logout</button>       
    </div>
</div>