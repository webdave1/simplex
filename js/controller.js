app.controller('cmsController', function($scope, $http, $location, TemplateService, DataService, SettingService, TemplateInfoService, ImageService){
        
    var settings = '';
    $scope.imageTemplates = ['img','thumbnail'];
    $scope.editId=-1;
    $scope.newTextElement='BlaBlaBla';
    $scope.nextImage='';
    $scope.nextSection = '';
    $scope.nextImagedata = 'HiHaho';
                
     $scope.newTemplate ={
            Headline: 'ToDo',
            subtitle: 'ToDo',
            text: new Array(),
            author: 'ToDo',
            date: new Date()  
        };
        
        
    SettingService.getSettings().then(function(response){			
        settings = response.data;
        $scope.SiteTitle = settings.SiteTitle;
        $scope.Head = settings.SideHead;
        $scope.Foot = settings.SideFoot;
        $scope.Author = settings.author;
    });
    
    $scope.login = function (val1, val2) {
        $http({
            method: "POST",
            url: "php/business.php",
            data: {
                cmd:"login",
                username:val1, 
                password:val2
            }
        }).success(function(data, status, headers, config) {
            $scope.newTemplate.author = val1;
            (data == 1)?$location.path("/setting"):$location.path("/login");
        });
    };
    
    $scope.signup = function (val1, val2) {
        $http({
            method: "POST",
            url: "php/business.php",
            data: {
                cmd:"signup",
                username:val1, 
                password:val2
            }
        }).success(function(data, status, headers, config) {
                if(data == 1)$location.path("/login");
        });
    };
    
    $scope.logout = function () {
        $http({
            method: "POST",
            url: "php/business.php",
            data: {
                cmd:"logout",
                username:"", 
                password:""
            }
        }).success(function(data, status, headers, config) {
                $location.path("/");
        });
    };
    
    $scope.addSection = function(type,val1,val2,sID){
        var text = val1;
        var src = "img/" + val2;
        if(sID != -1)console.log(sID + " => " + $scope.newTemplate.text[sID]);
        if($scope.imageTemplates.indexOf(type) != -1){
            if(sID == -1)
                $scope.newTemplate.text.push({content_type: type,data:src,data2:text});
            else{
                $scope.newTemplate.text[sID].content_type = type;
                $scope.newTemplate.text[sID].data = src;
                $scope.newTemplate.text[sID].data2 = text;
            }
        }else{
            if(sID == -1)
                $scope.newTemplate.text.push({content_type: type,data:text});
            else{
                $scope.newTemplate.text[sID].content_type = type;
                $scope.newTemplate.text[sID].data = text;
            }
        }
        
        if(sID != '')console.log($scope.newTemplate.text[sID]);
    };
    
    $scope.deleteSection = function(sId){
        console.log(sId);
        $scope.newTemplate.text.splice(sId, 1);
    };
    
    $scope.saveSection = function(){ 
        $scope.posts.push($scope.newTemplate);
        console.log($scope.posts);    
        $http({
            method: "POST",
            url: "php/business.php",
            data:{
                cmd:"update",
                newpost:$scope.posts
            }
        }).success(function(data, status, headers, config) {
            console.log(data);
//            DataService.getData().then(function (response) {
//                $scope.posts = response.data;
//            });
        });
        
    };
    
    $scope.updateImageList = function(){
        ImageService.getImageList().then(function (response){
            $scope.imageList = response.data;
        });
    };
    
    TemplateService.getTemplates().then(function (response) {
        $scope.templateList =  response.data;
    });
    
    TemplateInfoService.getTemplateInfos().then(function (response) {
        $scope.templateInfos =  response.data;
    });
    
    DataService.getData().then(function (response) {
        $scope.posts = response.data;
    });
    
    ImageService.getImageList().then(function (response){
        $scope.imageList = response.data;
    });
});

app.controller('PostDetailCtrl', ['$scope', '$routeParams',
  function($scope, $routeParams) {
    $scope.postId = $routeParams.postId;
  }]);
  
  app.controller('SettingController', function($scope, $http, $location, TemplateService, DataService, SettingService){
});
  