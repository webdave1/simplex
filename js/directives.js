/**
 * custom directives
 */

app.directive('pageNavigation', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/nav.tpl.html'
    }
});
app.directive('pageHeader', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/pagehead.tpl.html'
    }
});
app.directive('pageFooter', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/pagefoot.tpl.html'
    }
});
app.directive('cmsSettings', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/cmssettings.tpl.html'
    };
});
app.directive('imageUploader', function(){
    return {
        restrict: 'E',
        templateUrl: 'php/img_upload.html'
    };
});
app.directive('contentItem', function ($compile, TemplateService) {
    
    var linker = function (scope, element) {
        scope.rootDirectory = 'images/';
        TemplateService.getTemplates().then(function (response) {
            var templates = response.data;
            element.html(templates[scope.content.content_type]);

            $compile(element.contents())(scope);
        });
    };

    return {
        restrict: 'E',
        link: linker,
        scope: {
            content: '='
        }
    };
});
app.directive('demoItem', function ($compile, DummyTemplateService) {
    
    var linker = function (scope, element) {
        scope.rootDirectory = 'images/';
        DummyTemplateService.getDummyTemplates().then(function (response) {
            var templates = response.data;
            element.html(templates[scope.content.content_type]);

            $compile(element.contents())(scope);
        });
    };

    return {
        restrict: 'E',
        link: linker,
        scope: {
            content: '='
        }
    };
});
app.directive('postPreview', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/postPreview.tpl.html',
        scope: {
            content: '='
        }
    };
});
