var app = angular.module('cms', ['ngRoute', 'ngResource']);
app.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
});

app.config(function($routeProvider){
	$routeProvider		
		.when ('/home',
				{
					templateUrl: 'partials/start.html'
				})
		.when ('/post/:postId',
				{
					templateUrl: 'partials/post.html',
                                        controller: 'PostDetailCtrl'
				})	
		.when ('/contact',
				{
					templateUrl: 'partials/contact.html'
				})	
		.when ('/about',
				{
					templateUrl: 'partials/about.html'
				})	
		.when ('/setting',
				{
					templateUrl: 'php/editor.php',
                                        controller: 'SettingController'
				})	
		.when ('/login',
				{
					templateUrl: 'partials/login.tpl.html'
				})			
		.otherwise ({ redirectTo: '/home' });		
});


