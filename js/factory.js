
app.constant('URL', 'content/');

app.factory('DataService', function ($http, URL) {
    var getData = function () {
        return $http.get(URL + 'posts.json');
    };

    return {
        getData: getData
    };
});

app.factory('TemplateService', function ($http, URL) {
    var getTemplates = function () {
        return $http.get(URL + 'templates.json');
    };

    return {
        getTemplates: getTemplates
    };
});

app.factory('DummyTemplateService', function ($http, URL) {
    var getDummyTemplates = function () {
        return $http.get(URL + 'dummyTemplates.json');
    };

    return {
        getDummyTemplates: getDummyTemplates
    };
});

app.factory('TemplateInfoService', function ($http, URL) {
    var getTemplateInfos = function () {
        return $http.get(URL + 'templatesInfo.json');
    };

    return {
        getTemplateInfos: getTemplateInfos
    };
});

app.factory('SettingService', function ($http, URL) {
    var getSettings = function () {
        return $http.get(URL + 'sitesettings.json');
    };

    return {
        getSettings: getSettings
    };
});

app.factory('ImageService', function ($http, URL) {
    var getImageList = function () {
        return $http.get(URL + 'imageList.php');
    };

    return {
        getImageList: getImageList
    };
});